FROM alpine

RUN apk add --no-cache jq httpie gawk bash curl coreutils

RUN http get https://raw.githubusercontent.com/coursehero/slacktee/master/slacktee.sh > /usr/bin/slacktee && \
    chmod +x /usr/bin/slacktee 

COPY ./bin/* /usr/bin/

COPY Dockerfile docker-entrypoint.sh VERSION /

RUN adduser -S -g users app

USER app

ENTRYPOINT [ "/docker-entrypoint.sh" ]

