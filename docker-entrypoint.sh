#!/usr/bin/env bash

set -eo pipefail

[[ -n $DEBUG ]] && set -x

if [[ -f ./VERSION ]]; then
  version=$(cat ./VERSION)
else
  version="unknown"
fi

# just setup slacktee and the run updates-notifier
if [[ -z $SLACK_TOKEN ]]; then
  echo "Missing Slack configuration"
  exit 1
fi
echo "[info] setting slack token to send notification"
echo "webhook_url=$SLACK_TOKEN" > ~/.slacktee

if [[ -n $SLACK_CHANNEL ]]; then
  echo "[info] setting notification channel to $SLACK_CHANNEL"
  echo "channel=${SLACK_CHANNEL}" >> ~/.slacktee
fi

icon=${SLACK_ICON:-"https://avatars.slack-edge.com/2019-04-10/592704632323_94e18badb9732c11008e_36.jpg"}
echo "[info] setting icon of slack notification to $icon"
echo "icon=${icon}" >> ~/.slacktee

username=${SLACK_USERNAME:-"Marvin"}
echo "[info] setting username of slack notification to $username"
echo "username=\"${username}\"" >> ~/.slacktee

# check that slacktee setup is ok or die immediatly
echo ":checkered_flag: Watching <https://developers.italia.it/it/software/|Developers Italia> software for updates" | \
  slacktee --plain-text --attachment --field updates-notifier "version $version"

echo "[info] starting infinite loop"
while true; do
  updates-notifier | slacktee --plain-text --attachment good --icon https://developers.italia.it/favicon.ico
  sleep ${RUN_INTERVAL:-3600}
done

